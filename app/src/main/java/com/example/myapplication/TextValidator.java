package com.example.myapplication;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

public abstract class TextValidator implements TextWatcher , View.OnFocusChangeListener {

    private final TextView textView;
    public TextValidator(TextView textview){
        this.textView = textview;
    }
    public abstract void validate(TextView textview , String text);

    @Override
    final public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    final public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    final public void afterTextChanged(Editable s) {
        String text = textView.getText().toString();
        validate(textView,text);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        String text = textView.getText().toString();
        if(!hasFocus){
            validate(textView,text);
        }
    }
}

