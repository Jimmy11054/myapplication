package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.annotation.NonNull;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.MotionEvent;
import android.content.Context;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast;
import android.text.TextUtils;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;
 
import java.util.Calendar;

public class EmailSignUpActivity extends AppCompatActivity {
    TextInputLayout textInputLayoutBirthday;
    TextInputLayout textInputLayoutEmail;
    Button buttonSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_sign_up);
        textInputLayoutEmail = (TextInputLayout) findViewById(R.id.textInputLayoutEmail);
        textInputLayoutBirthday = (TextInputLayout) findViewById(R.id.til_Birthday);
        textInputLayoutBirthday.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new myDatePickerDialog(EmailSignUpActivity.this, v).show();
            }
        });

        TextValidator emailValidator = new TextValidator(textInputLayoutEmail.getEditText()) {
            @Override
            public void validate(TextView textview, String text) {
                if (!TextUtils.isEmpty(text)) {
                    textInputLayoutEmail.setError(null);
                } else {
                    textInputLayoutEmail.setError(getString(R.string.error_empty_email));

                }
            }
        };

        textInputLayoutEmail.getEditText().addTextChangedListener(emailValidator);
        textInputLayoutEmail.getEditText().setOnFocusChangeListener(emailValidator);
        /**結束宣告使用TextValidator**/

        buttonSignUp = findViewById(R.id.button_sign_up);
        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textInputLayoutEmail.getEditText().getError() != null || textInputLayoutEmail.getEditText().getText().length() == 0) {
                    Toast.makeText(EmailSignUpActivity.this, R.string.error_empty_email, Toast.LENGTH_SHORT).show();
                } else {
                }
            }

        });
        public class myDatePickerDialog extends DatePickerDialog {
            public myDatePickerDialog(@NonNull Context context, View view) {
                super(context, new DatePickerDialogListener(view), Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH),
                        Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
            }
        }
        public class DatePickerDialogListener implements DatePickerDialog.OnDateSetListener {
            private TextView textView;

            public DatePickerDialogListener(View view) {
                this.textView = (TextView) view;
            }

            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String date = year + "-" + (month + 1) + "-" + dayOfMonth;
                textView.setText(date);
            }
        }
    }
}